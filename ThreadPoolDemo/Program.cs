﻿using System;
using System.ComponentModel;
using System.Threading;

namespace ThreadPoolDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            ThreadParallelTask();

            Console.Write("\r\n..............................\r\n\r\n");

            BackgroudWorkerParallelTask();

            Console.Write("\r\n\r\n\r\n>>> Press key to close <<<");
            Console.ReadKey();
        }

        public static void ThreadParallelTask()
        {
            var count = 5;
            var threads = new Thread[count];

            for (int i = 0; i < count; i++)
            {
                threads[i] = new Thread(delegate ()
                {
                    Thread.Sleep(1000);
                })
                { Name = string.Concat("thread_", i.ToString()), IsBackground = true };
            }

            Array.ForEach(threads, delegate (Thread t)
            {
                t.Start();
                Console.WriteLine(" {0} => Start", t.Name);
            });

            Array.ForEach(threads, delegate (Thread t)
            {
                t.Join();
                Console.WriteLine(" {0} => Join", t.Name);
            });
        }

        public static void BackgroudWorkerParallelTask()
        {
            var thread = new Thread(new ThreadStart(() =>
            {
                var count = 5;
                var backgroudworkers = new BackgroundWorker[count];

                for (int i = 0; i < count; i++)
                {
                    var idx = i.ToString();

                    backgroudworkers[i] = new BackgroundWorker();

                    backgroudworkers[i].DoWork += (s, e) =>
                    {
                        Console.WriteLine(" bgw_{0} => Start", idx);
                        Thread.Sleep(1000);
                    };

                    backgroudworkers[i].RunWorkerCompleted += (s, e) =>
                    {
                        Console.WriteLine(" bgw_{0} => Completed", idx);
                        ((BackgroundWorker)s).Dispose();
                    };
                }

                Array.ForEach(backgroudworkers, delegate (BackgroundWorker bgw)
                {
                    bgw.RunWorkerAsync();
                });

                Array.ForEach(backgroudworkers, delegate (BackgroundWorker bgw)
                {
                    while (bgw.IsBusy) Thread.Sleep(100);
                });
            }));

            thread.Start();
            thread.Join();
        }
    }
}
